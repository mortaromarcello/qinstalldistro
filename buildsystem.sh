#!/usr/bin/env bash
# redirect stdout/stderr to a file
LOG=
if [  ${LOG} = "true" ]; then
    LOG_FILE=log.txt
    exec >> $LOG_FILE 2>&1
fi
set -xv
PWD=$(pwd)
WORK=${PWD}/work
ISO=${WORK}/ISO
ISONAMEFILE=devuan.iso
DEBOOTSTRAP_DIR=${PWD}/debootstrap
DEBOOTSTRAP=${DEBOOTSTRAP_DIR}/debootstrap
APT_OPTIONS="--yes --show-progress"
PRESEED="keyboard-configuration keyboard-configuration/layout   select\n
libc6   glibc/kernel-not-supported  note\n
libc6:i386  glibc/kernel-not-supported  note\n
hddtemp hddtemp/SUID_bit    boolean false\n
console-setup   console-setup/fontsize-text47   select  8x16\n
devuan-baseconf devuan-baseconf/suite   string  ascii\n
locales locales/default_environment_locale  select  it_IT.UTF-8\n
ca-certificates ca-certificates/trust_new_crts  select  yes\n
keyboard-configuration  keyboard-configuration/store_defaults_in_debconf_db boolean true\n
base-passwd base-passwd/group-remove    boolean true\n
locales locales/locales_to_be_generated multiselect it_IT.UTF-8 UTF-8\n
hddtemp hddtemp/syslog  string  0\n
hddtemp hddtemp/daemon  boolean false\n
tasksel tasksel/first   multiselect\n
keyboard-configuration  keyboard-configuration/unsupported_layout   boolean true\n
console-setup   console-setup/codeset47 select\n
tzdata  tzdata/Zones/Europe select  Rome\n
console-setup   console-setup/fontsize  string  8x16\n
base-passwd base-passwd/user-change-shell   boolean true\n
ucf ucf/changeprompt_threeway   select  keep_current\n
update-inetd    update-inetd/ask-remove-entries boolean false\n
base-passwd base-passwd/user-move   boolean true\n
wicd-daemon wicd/users  multiselect\n
tzdata  tzdata/Areas    select  Etc\n
base-passwd base-passwd/group-add   boolean true\n
libpaper1   libpaper/defaultpaper   select  a4\n
libpaper1:i386  libpaper/defaultpaper   select  a4\n
phonon  phonon-backend-null/isnt_functional note\n
phonon:i386 phonon-backend-null/isnt_functional note\n
update-inetd    update-inetd/ask-several-entries    boolean true\n
keyboard-configuration  keyboard-configuration/altgr    select  Right Alt (AltGr)\n
keyboard-configuration  keyboard-configuration/variant  select\n
keyboard-configuration  keyboard-configuration/switch   select  No temporary switch\n
base-passwd base-passwd/user-change-gecos   boolean true\n
keyboard-configuration  keyboard-configuration/unsupported_config_layout    boolean true\n
base-passwd base-passwd/user-change-uid boolean true\n
keyboard-configuration  keyboard-configuration/toggle   select  Alt+Shift\n
keyboard-configuration  keyboard-configuration/variantcode  string\n
keyboard-configuration  keyboard-configuration/unsupported_options  boolean true\n
update-inetd    update-inetd/ask-disable-entries    boolean false\n
ucf ucf/show_diff   note\n
tasksel tasksel/desktop multiselect\n
update-inetd    update-inetd/ask-entry-present  boolean true\n
sddm    sddm/daemon_name    string  /usr/bin/sddm\n
dictionaries-common dictionaries-common/default-wordlist    select  english (Webster's Second International English wordlist)\n
xserver-xorg-legacy xserver-xorg-legacy/xwrapper/actual_allowed_users   string  console\n
base-passwd base-passwd/group-move  boolean true\n
base-passwd base-passwd/user-change-home    boolean true\n
sane-utils  sane-utils/saned_run    boolean false\n
ca-certificates ca-certificates/new_crts    multiselect\n
libc6   glibc/restart-services  string\n
libc6:i386  glibc/restart-services  string\n
dictionaries-common dictionaries-common/default-ispell  select\n
sysvinit-core   sysvinit/hurd-fix-inittab   boolean\n
keyboard-configuration  keyboard-configuration/ctrl_alt_bksp    boolean false\n
keyboard-configuration  keyboard-configuration/layoutcode   string\n
kdesudo kdesudo/kdesu   boolean false\n
libssl1.1:i386  libssl1.1/restart-services  string\n
fontconfig-config   fontconfig/subpixel_rendering   select  Automatic\n
tzdata  tzdata/Zones/Etc    select  UTC\n
keyboard-configuration  keyboard-configuration/model    select\n
dictionaries-common dictionaries-common/selecting_ispell_wordlist_default   note\n
keyboard-configuration  keyboard-configuration/optionscode  string\n
console-setup   console-setup/store_defaults_in_debconf_db  boolean false\n
base-passwd base-passwd/user-remove boolean true\n
libssl1.0.2:i386    libssl1.0.2/restart-services    string\n
dictionaries-common dictionaries-common/old_wordlist_link   boolean true\n
libpam0g:i386   libpam0g/restart-services   string\n
ucf ucf/changeprompt    select  keep_current\n
base-passwd base-passwd/group-change-gid    boolean true\n
base-passwd base-passwd/user-add    boolean true\n
console-setup   console-setup/fontface47    select  Fixed\n
dictionaries-common dictionaries-common/ispell-autobuildhash-message    note\n
debconf debconf/frontend    select  Dialog\n
console-setup   console-setup/charmap47 select  UTF-8\n
libpam-runtime  libpam-runtime/override boolean false\n
hddtemp hddtemp/interface   string  127.0.0.1\n
libc6   glibc/upgrade   boolean true\n
libc6:i386  glibc/upgrade   boolean true\n
sane-utils  sane-utils/saned_scanner_group  boolean true\n
man-db  man-db/auto-update  boolean true\n
devuan-baseconf devuan-baseconf/components  string  main\n
tasksel tasksel/tasks   multiselect\n
libpam-runtime  libpam-runtime/profiles multiselect unix, gnome-keyring, consolekit, capability\n
console-setup   console-setup/codesetcode   string  Lat15\n
devuan-baseconf devuan-baseconf/note    note\n
keyboard-configuration  keyboard-configuration/modelcode    string\n
base-passwd base-passwd/user-change-gid boolean true\n
man-db  man-db/install-setuid   boolean false\n
cups-bsd    cups-bsd/setuplpd   boolean false\n
adduser adduser/homedir-permission  boolean true\n
fontconfig-config   fontconfig/enable_bitmaps   boolean false\n
keyboard-configuration  keyboard-configuration/unsupported_config_options   boolean true\n
hddtemp hddtemp/port    string  7634\n
unattended-upgrades unattended-upgrades/enable_auto_updates boolean true\n
#sddm shared/default-x-display-manager  select  sddm\n
debconf debconf/priority select high\n
libc6   libraries/restart-without-asking    boolean false\n
libc6:i386  libraries/restart-without-asking    boolean false\n
libpam0g:i386   libraries/restart-without-asking    boolean false\n
dash    dash/sh boolean true\n
keyboard-configuration  keyboard-configuration/compose  select  No compose key\n
xserver-xorg-legacy xserver-xorg-legacy/xwrapper/allowed_users  select  Console Users Only\n
phonon4qt5  phonon4qt5-backend-null/isnt_functional note\n
phonon4qt5:i386 phonon4qt5-backend-null/isnt_functional note\n
console-setup   console-setup/fontsize-fb47 select  8x16\n
fontconfig-config   fontconfig/hinting_type select  Native\n
"
MY_CHROOT=${WORK}/CHROOT
trap '{ echo interrupt ; umount -lf ${MY_CHROOT}/dev/pts &>/dev/null ; umount -lf {MY_CHROOT}/sys &>/dev/null ; umount -lf ${MY_CHROOT}/proc &>/dev/null ; exit 255 ; }' SIGINT SIGTERM
ARCH=i386
DIST=ascii
MIRROR=http://auto.mirror.devuan.org/merged
INCLUDE=mc,vim,sudo
ROOT_PASSWORD=toor
USER_PASSWORD=devuan
USERNAME=devuan
CRYPT_ROOT_PASSWORD=$(perl -e 'print crypt($ARGV[0], "password")' "${ROOT_PASSWORD}")
CRYPT_USER_PASSWORD=$(perl -e 'print crypt($ARGV[0], "password")' "${USER_PASSWORD}")
CONSOLE="binutils pciutils usbutils console-setup net-tools wireless-tools rpl parted rsync grub2-common grub-pc-bin ntp telnet ftp cu screen minicom picocom proftpd-basic connman connman-vpn traceroute apache2"
PLASMA_DESKTOP="kdm plasma-desktop dolphin konsole ark kwrite krita cmst kaffeine calligra calligra-l10n-it putty remmina remmina-plugin-vnc firefox-esr firefox-esr-l10n-it okular gwenview sflphone-kde cutecom gtkterm clamtk"
LXDE_DESKTOP="lightdm lxde-core lxtask lxlauncher cmst xorg xserver-xorg-video-all xserver-xorg-input-all remmina remmina-plugin-vnc firefox-esr firefox-esr-l10n-it evince-gtk gpicview lxappearance lxterminal lxde-icon-theme xarchiver mplayer-gui sflphone-gnome putty cutecom gtkterm clamtk blueman"
XFCE_DESKTOP="lightdm xfce4 xfce4-goodies xfce4-power-manager gtk3-engines-xfce evince-gtk xfce4-terminal xfce4-pulseaudio-plugin cmst remmina remmina-plugin-vnc firefox-esr firefox-esr-l10n-it mplayer-gui sflphone-gnome putty cutecom gtkterm clamtk blueman"
DISK_CONSOLE_UTILITY="testdisk blkreplay clonezilla diskscan disktype exfat-fuse gdisk gpart lvm2 mtools dbench stress"
DISK_DESKTOP_UTILITY="gparted gsmartcontrol fslint"
DEVEL="git cmake make g++ libparted0-dev build-essential shellcheck cgdb strace"
DEVEL_DESKTOP="geany geany-plugins qtcreator qtbase5-dev"
MULTIMEDIA="musescore vlc"
NETWORK_CONSOLE_UTILITY="arp-scan nmap netcat tcpdump dsniff ngrep bro ntopng mtr-tiny cdpr"
NETWORK_DESKTOP_UTILITY="zenmap etherape filezilla ekiga gvncviewer"
EMULATORS="qemu-kvm virt-manager ovmf virtualbox wine winetricks winbind wine-binfmt dosbox"
PENETRATION_CONSOLE_UTILITY="dcfldd forensics-all autopsy samdump2 polenum ophcrack-cli"
PENETRATION_DESKTOP_UTILITY="ophcrack dissy"
DESKTOP=false
PACKAGES=${CONSOLE}
SLIM=/usr/bin/slim
KDM=/usr/bin/kdm
SDDM=/usr/bin/sddm
LIGHTDM=/usr/sbin/lightdm
LXDM=/usr/bin/lxdm
DEFAULT_DISPLAY_MANAGER=$KDM
#LOCALE="it_IT.UTF-8 UTF-8"
LANG="it_IT.UTF-8"
XKBMODEL="pc105"
XKBLAYOUT="it"
XKBVARIANT=""
XKBOPTIONS=""
HOSTNAME="devuan"
ADD_GROUPS="cdrom,floppy,audio,dip,video,plugdev,lp,dialout,netdev"
TIMEZONE="Europe/Rome"
SHELL_USER="/bin/bash"
SQUASHFSFILE="filesystem.squashfs"
ISOLINUX_MENU="UI menu.c32\n
prompt 0\n
menu title Devuan Live\n
timeout 300\n
label Devuan Live\n
menu label ^Devuan Live\n
menu default\n
kernel /boot/vmlinuz\n
append initrd=/boot/initrd.img boot=live live-media-path=/boot persistence\n
label hdt\n
menu label ^Hardware Detection Tool (HDT)\n
kernel hdt.c32\n
text help\n
HDT displays low-level information about the systems hardware.\n
endtext\n
label memtest86+\n
menu label ^Memory Failure Detection (memtest86+)\n
kernel /boot/isolinux/memtest\n"

SDDM_CONF="[Autologin]\n
Relogin=false\n
Session=plasma.desktop\n
User=${USERNAME}\n
[General]\n
HaltCommand=/sbin/poweroff\n
InputMethod=\n
Numlock=none\n
RebootCommand=/sbin/reboot\n
[Theme]\n
Current=debian-theme\n
CursorTheme=\n
EnableAvatars=true\n
FacesDir=/usr/share/sddm/faces\n
ThemeDir=/usr/share/sddm/themes\n
[Users]\n
DefaultPath=/bin:/usr/bin\n
HideShells=\n
HideUsers=\n
MaximumUid=60000\n
MinimumUid=1000\n
RememberLastSession=true\n
RememberLastUser=true\n
[Wayland]
EnableHiDPI=false\n
SessionCommand=/usr/share/sddm/scripts/wayland-session\n
SessionDir=/usr/share/wayland-sessions\n
SessionLogFile=.local/share/sddm/wayland-session.log\n
[X11]\n
DisplayCommand=/usr/share/sddm/scripts/Xsetup\n
DisplayStopCommand=/usr/share/sddm/scripts/Xstop\n
EnableHiDPI=false\n
MinimumVT=7\n
ServerArguments=-nolisten tcp\n
ServerPath=/usr/bin/X\n
SessionCommand=/etc/sddm/Xsession\n
SessionDir=/usr/share/xsessions\n
SessionLogFile=.local/share/sddm/xorg-session.log\n
UserAuthFile=.Xauthority\n
XauthPath=/usr/bin/xauth\n
XephyrPath=/usr/bin/Xephyr\n"

#-----------------------------------------------------------------------

function help() {
    echo -e "
${0} <opzioni>
Crea una Live Devuan ISO.
    -a | --arch <architecture>    :Archittetura della CPU. (default 'i386').
    -b | --build-iso              :Crea solamente la ISO.
    -d | --distro                 :Tipo di distro jessie,ascii,ceres. (default 'ascii')
    -D | --desktop                :Installa i pacchetti per il Desktop.
    --disk-console-utils          :Installa i pacchetti utilità dischi per console.
    --disk-desktop-utils          :Installa i pacchetti utilità disco per desktop.
    --devel                       :Installa i pacchetti per lo sviluppo software.
    --devel-desktop               :Installa i pachetti per lo sviluppo software desktop.
    -F | --full                   :Installa tutti i pacchetti.
    -h | --help                   :Stampa questa messaggio.
    -L | --language               :Lingua (default 'it_IT.UTF-8').
    --lxde                        :Installa lxde.
    -n | --hostname               :Nome dell'host.
    -s | --makesquashfs           :Crea il file system compresso filesystem.squashfs
    -u | --user                   :Nome utente.
    --xfce                        :Installa xfce.
"
}

function check_network() {
    ping -c 3 www.google.com &>/dev/null
    if [ "$?" != "0" ]; then
        echo "Network irraggiungibile"
        exit -1
    fi
}

function init() {
    check_root
    check_network
    if [ -d "${MY_CHROOT}" ]; then
        rm -rvf "${WORK}"
    fi
    apt install -y rpl git syslinux isolinux squashfs-tools genisoimage memtest86+
    mkdir -vp "${MY_CHROOT}"
    mkdir -vp "${ISO}"/boot
    if [ -d "${DEBOOTSTRAP_DIR}" ]; then
        rm -rvf "${DEBOOTSTRAP_DIR}"
    fi
    git clone https://git.devuan.org/devuan-packages/debootstrap.git "${DEBOOTSTRAP_DIR}"
}

function check_root() {
    if [ ${UID} != 0 ]; then
        echo "Devi essere root per eseguire questo script."
        exit
    fi
}

function check_options() {
    if [ "${ARCH}" = "i386" ]; then
        LINUX_IMAGE=linux-image-686-pae
    else
        LINUX_IMAGE=linux-image-amd64
    fi
    if [ "${ARCH}" != "i386" ] && [ "${ARCH}" != "amd64" ]; then
        echo "ARCH=${ARCH} non è ammesso"
        exit 255
    fi
    if [ "${DIST}" != "jessie" ] && [ "${DIST}" != "ascii" ] && [ "${DIST}" != "ceres" ]; then
    echo "DIST=${DIST} non è ammesso"
        exit 255
    fi
    if [ "${DESKTOP}" = "true" ]; then
        ADD_GROUPS="${ADD_GROUPS},bluetooth"
    fi
}

function root_password() {
    cat << EOF | chroot "${MY_CHROOT}"
echo "root:${CRYPT_ROOT_PASSWORD}" | chpasswd -e
EOF
}

function add_user() {
    if chroot "${MY_CHROOT}" useradd -G ${ADD_GROUPS} -s ${SHELL_USER} -u 1000 -o -m -p "${CRYPT_USER_PASSWORD}" "${USERNAME}"; then
        echo "User has been added to system!" 
    else
        echo "Failed to add a user!";
        exit
    fi
}

function set_hostname() {
    cat > "${MY_CHROOT}"/etc/hostname <<EOF
${HOSTNAME}
EOF
    cat > "${MY_CHROOT}"/etc/hosts <<EOF
127.0.0.1       localhost
127.0.1.1       ${HOSTNAME}
::1             localhost ip6-localhost ip6-loopback
fe00::0         ip6-localnet
ff00::0         ip6-mcastprefix
ff02::1         ip6-allnodes
ff02::2         ip6-allrouters
EOF
}

function install_packages() {
    cat << EOF | chroot "${MY_CHROOT}"
apt ${APT_OPTIONS} install ${PACKAGES}
EOF
}

function install_grub() {
    cat << EOF | chroot "${MY_CHROOT}" 
apt -d install grub-pc
cd /var/cache/apt/archives/
dpkg --unpack grub-pc*.deb
rm -vf grub-pc*.deb
EOF
}

function bootstrap() {
    export DEBOOTSTRAP_DIR=$DEBOOTSTRAP_DIR && ${DEBOOTSTRAP} --no-check-gpg --include=${INCLUDE} --arch ${ARCH} ${DIST} "${MY_CHROOT}" ${MIRROR}
}

function set_preseed_conf() {
    echo -e "${PRESEED}" > "${MY_CHROOT}"/preseed
    chroot "${MY_CHROOT}" debconf-set-selections ./preseed
    rm -vf "${MY_CHROOT}"/preseed
}

function set_repository() {
    if [ -e "${MY_CHROOT}"/etc/apt/sources.list ]; then
        if [ "${DIST}" != "jessie" ]; then
            echo -e "\ndeb ${MIRROR} jessie main contrib non-free\ndeb ${MIRROR} ${DIST} main contrib non-free\n" > "${MY_CHROOT}"/etc/apt/sources.list
        else
            echo -e "\ndeb ${MIRROR} jessie main contrib non-free\n" > "${MY_CHROOT}"/etc/apt/sources.list
        fi
    fi
}

function upgrade() {
    cat << EOF | chroot "${MY_CHROOT}"
apt update && apt -y upgrade
EOF
}

function install_locales() {
    #set_locale
    chroot "${MY_CHROOT}" apt -y install locales
}

#function set_locale() {
#   echo "$LOCALE" > ${MY_CHROOT}/etc/locale.gen
    #chroot ${MY_CHROOT} locale-gen
    #chroot ${MY_CHROOT} update-locale LANG=${LANG}
#}

function set_keyboard() {
    echo -e "XKBMODEL=${XKBMODEL}\nXKBLAYOUT=${XKBLAYOUT}\nXKBVARIANT=${XKBVARIANT}\nXKBOPTIONS=${XKBOPTIONS}\n" > "${MY_CHROOT}"/etc/default/keyboard
}

function set_timezone() {
    cat > "${MY_CHROOT}"/etc/timezone <<EOF
${TIMEZONE}
EOF
}

#function set_default_dm() {
#   mkdir -vp ${MY_CHROOT}/etc/X11
#   echo -e "${DEFAULT_DISPLAY_MANAGER}" > ${MY_CHROOT}/etc/X11/default-display-manager
#}

function mount_proc() {
    cat << EOF | chroot "${MY_CHROOT}"
mount -t proc none /proc
EOF
}

function mount_sys() {
    cat << EOF | chroot "${MY_CHROOT}"
mount none -t sysfs /sys
EOF
}

function mount_devpts() {
    cat << EOF | chroot "${MY_CHROOT}"
mount none -t devpts /dev/pts
EOF
}

function umount_proc() {
    cat << EOF | chroot "${MY_CHROOT}"
umount -lf /proc
EOF
}

function umount_sys() {
    cat << EOF | chroot "${MY_CHROOT}"
umount -lf /sys
EOF
}

function umount_devpts() {
    cat << EOF | chroot "${MY_CHROOT}"
umount -lf /dev/pts
EOF
}

function fake_invokerc() {
    cp -v "${MY_WORK}"/usr/sbin/invoke-rc.d "${MY_WORK}"/usr/sbin/invoke-rc.d.orig
    echo -e "#!/bin/sh\n#Fake invoke-rc.d\n" > "${MY_WORK}"/usr/sbin/invoke-rc.d
    chmod +x "${MY_WORK}"/usr/sbin/invoke-rc.d
}

function restore_invokerc() {
    if [ -e "${MY_WORK}"/usr/sbin/invoke-rc.d.orig ]; then
        rm -v "${MY_WORK}"/usr/sbin/invoke-rc.d
        cp -v "${MY_WORK}"/usr/sbin/invoke-rc.d.orig "${MY_WORK}"/usr/sbin/invoke-rc.d
    fi
}

function add_sudo_user() {
    chroot "${MY_CHROOT}" gpasswd -a "${USERNAME}" sudo
    cat > "${MY_CHROOT}"/etc/sudoers.d/nopasswd <<EOF
${USERNAME} ALL=(ALL) NOPASSWD: ALL
EOF
}

function set_autologin_console() {
    LINE=$(grep "1:2345:respawn:/sbin/getty" "${MY_CHROOT}"/etc/inittab)
    rpl "${LINE}" "1:2345:respawn:/sbin/getty -a ${USERNAME} tty1" "${MY_CHROOT}"/etc/inittab
}

function set_autologin() {
    DM=$(cat "${MY_CHROOT}"/etc/X11/default-display-manager)
    if [ "${DM}" = "" ]; then
        return
    fi
    if [ "${DM}" = "${LIGHTDM}" ]; then
        LINE=$(grep "#autologin-user=" "${MY_CHROOT}"/etc/lightdm/lightdm.conf)
        rpl "${LINE}" "autologin-user=${USERNAME}" "${MY_CHROOT}"/etc/lightdm/lightdm.conf
    elif [ "${DM}" = "${SLIM}" ]; then
        LINE=$(grep "auto_login" "${MY_CHROOT}"/etc/slim.conf)
        rpl "${LINE}" "auto_login          yes" "${MY_CHROOT}"/etc/slim.conf
        LINE=$(grep "#default_user" "${MY_CHROOT}"/etc/slim.conf)
        rpl "${LINE}" "default_user          ${USERNAME}" "${MY_CHROOT}"/etc/slim.conf
    elif [ "${DM}" = "${KDM}" ]; then
        LINE=$(grep "#AutoLoginEnable=" "${MY_CHROOT}"/etc/kde4/kdm/kdmrc)
        rpl "${LINE}" "AutoLoginEnable=true" "${MY_CHROOT}"/etc/kde4/kdm/kdmrc
        LINE=$(grep "#AutoLoginUser=" "${MY_CHROOT}"/etc/kde4/kdm/kdmrc)
        rpl "${LINE}" "AutoLoginUser=${USERNAME}" "${MY_CHROOT}"/etc/kde4/kdm/kdmrc
    elif [ "${DM}" = "${LXDM}" ]; then
        LINE=$(grep "#autologin=" "${MY_CHROOT}"/etc/lxdm/lxdm.conf)
        rpl "${LINE}" "autologin=${USERNAME}" "${MY_CHROOT}"/etc/lxdm/lxdm.conf
    elif [ "${DEFAULT_DISPLAY_MANAGER}" = "${SDDM}" ]; then
        echo -e "${SDDM_CONF}" > "${MY_CHROOT}"/etc/sddm.conf
    fi
}

function install_kernel() {
    cat << EOF | chroot "${MY_CHROOT}"
apt ${APT_OPTIONS} install ${LINUX_IMAGE} firmware-linux amd64-microcode intel-microcode firmware-realtek firmware-iwlwifi bluez-firmware firmware-amd-graphics live-boot
EOF
}

function install_scripts() {
    git clone https://github.com/mortaromarcello/scripts.git "${PWD}"/scripts
    cp "${PWD}"/scripts/simple_install_distro.sh "${MY_CHROOT}"/usr/sbin/install_distro.sh
    chmod +x "${MY_CHROOT}"/usr/sbin/install_distro.sh
    rm -rfv "${PWD}"/scripts
}

function clean_system() {
    cat << EOF | chroot "${MY_CHROOT}"
apt --purge autoremove
apt clean
EOF
}

function create_squashfs() {
    mksquashfs "${MY_CHROOT}" "${ISO}"/boot/${SQUASHFSFILE} -info
    du -sx --block-size=1 "${MY_CHROOT}" | cut -f1 > "${ISO}"/boot/filesystem.size
}

function make_iso() {
    mkdir -vp "${ISO}"/boot
    cp -v "${MY_CHROOT}"/boot/vmlinuz-4.9.0* "${ISO}"/boot/vmlinuz
    cp -v "${MY_CHROOT}"/boot/initrd.img-* "${ISO}"/boot/initrd.img
    mkdir -vp "${ISO}"/boot/isolinux/
    echo -e "${ISOLINUX_MENU}" > "${ISO}"/boot/isolinux/isolinux.cfg
    (cp -v /usr/lib/ISOLINUX/isolinux.bin "${ISO}"/boot/isolinux/ && \
    cp -v /usr/lib/syslinux/modules/bios/menu.c32 "${ISO}"/boot/isolinux/ && \
    cp -v /usr/lib/syslinux/modules/bios/hdt.c32 "${ISO}"/boot/isolinux/ && \
    cp -v /usr/lib/syslinux/modules/bios/ldlinux.c32 "${ISO}"/boot/isolinux/ && \
    cp -v /usr/lib/syslinux/modules/bios/libutil.c32 "${ISO}"/boot/isolinux/ && \
    cp -v /usr/lib/syslinux/modules/bios/libmenu.c32 "${ISO}"/boot/isolinux/ && \
    cp -v /usr/lib/syslinux/modules/bios/libcom32.c32 "${ISO}"/boot/isolinux/ && \
    cp -v /usr/lib/syslinux/modules/bios/libgpl.c32 "${ISO}"/boot/isolinux/ && \
    cp -v /boot/memtest86+.bin "${ISO}"/boot/isolinux/memtest)
    cd "${ISO}" || exit
    genisoimage -rational-rock -volid "Devuan Live" -cache-inodes -joliet -full-iso9660-filenames -b boot/isolinux/isolinux.bin -c boot/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -output "${WORK}/${ISONAMEFILE}" "${ISO}"
}

function build_iso() {
    rm -rvf "${ISO}"
    rm -vf "${WORK}/${ISONAMEFILE}"
    mkdir -vp "${ISO}/boot"
    create_squashfs
    make_iso
}

function build_system() {
    init
    check_options
    bootstrap
    mount_proc
    mount_sys
    mount_devpts
    set_preseed_conf
    fake_invokerc
    set_repository
    upgrade
    install_locales
    set_keyboard
    set_timezone
    set_hostname
    root_password
    install_kernel
    install_packages
    add_user
    add_sudo_user
    if [ "${DESKTOP}" = "false" ]; then
        set_autologin_console
    else
        set_autologin
    fi
    install_scripts
    clean_system
    install_grub
    umount_devpts
    umount_sys
    umount_proc
    restore_invokerc
    create_squashfs
    make_iso
}

#-----------------------------------------------------------------------

until [ -z "${1}" ]
do
    case "${1}" in
        -a | --arch)
            shift
            ARCH="${1}"
            ;;
        -b | --build-iso)
            shift
            BUILD_ISO=true
            ;;
        -d | --distro)
            shift
            DIST="${1}"
            ;;
        -D | --desktop)
            shift
            PACKAGES="${PACKAGES} ${PLASMA_DESKTOP}"
            DESKTOP=true
            ;;
        --disk-console-utils)
            shift
            PACKAGES="${PACKAGES} ${DISK_CONSOLE_UTILITY}"
            ;;
        --disk-desktop-utils)
            shift
            PACKAGES="${PACKAGES} ${DISK_DESKTOP_UTILITY}"
            ;;
        --devel)
            shift
            PACKAGES="${PACKAGES} ${DEVEL}"
            ;;
        --devel-desktop)
            shift
            PACKAGES="${PACKAGES} ${DEVEL_DESKTOP}"
            ;;
        -F | --full)
            shift
            PACKAGES="${PACKAGES} ${PLASMA_DESKTOP} ${DEVEL} ${DEVEL_DESKTOP} ${MULTIMEDIA} ${DISK_CONSOLE_UTILITY} ${DISK_DESKTOP_UTILITY} ${NETWORK_CONSOLE_UTILITY} ${NETWORK_DESKTOP_UTILITY} ${EMULATORS} ${PENETRATION_CONSOLE_UTILITY} ${PENETRATION_DESKTOP_UTILITY}"
            DESKTOP=true
            ;;
        -h | --help)
            shift
            help
            exit
            ;;
        -L | --language)
            shift
            LANG="${1}"
            ;;
        --lxde)
            shift
            PACKAGES="${PACKAGES} ${LXDE_DESKTOP}"
            DESKTOP=true
            ;;
        -n | --hostname)
            shift
            HOSTNAME="${1}"
            ;;
        -o | --apt-options)
            shift
            APT_OPTIONS="${APT_OPTIONS} ${1}"
            ;;
        -s | --makesquashfs)
            shift
            mksquashfs "${MY_CHROOT}" "${1}" -info
            exit
            ;;
        -u | --user)
            shift
            USERNAME=${1}
            ;;
        --xfce)
            shift
            PACKAGES="${PACKAGES} ${XFCE_DESKTOP}"
            DESKTOP=true
            ;;
        *)
            shift
            ;;
    esac
done
if [ "${BUILD_ISO}" = "true" ]; then
    build_iso
else
    build_system
fi
