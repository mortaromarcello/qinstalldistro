#include "config.h"
#include "qinstalldistro.h"
#include <QTranslator>
#include <QMessageBox>
#include <signal.h>
#include <stdio.h>
#include <QLocale>

void signalHandler(int signal);

int main(int argc, char** argv)
{
    QInstallDistro app(argc, argv);
    if (isUserRoot()) {
        fprintf(stderr, qstr2str(QObject::tr("you are root!\n")));
    }
    else {
        fprintf(stderr, qstr2str(QObject::tr("you are not root...\n")));
        app.messageInfo(QObject::tr("You are not root!"));
        exit(-1);
    }
    int ret;
    // configure app's reaction to OS signals
    struct sigaction act, oact;
    memset((void*)&act, 0, sizeof(struct sigaction));
    memset((void*)&oact, 0, sizeof(struct sigaction));
    act.sa_flags = 0;
    act.sa_handler = &signalHandler;
    sigaction(SIGINT, &act, &oact);
    sigaction(SIGKILL, &act, &oact);
    sigaction(SIGQUIT, &act, &oact);
    sigaction(SIGSTOP, &act, &oact);
    sigaction(SIGTERM, &act, &oact);
    sigaction(SIGSEGV, &act, &oact);
    
    QTranslator translator;
    QLocale locale = QLocale::system();
    QLocale::setDefault(locale);
    fprintf(stderr, qstr2str(locale.bcp47Name()));
    fprintf(stderr, qstr2str(QString(qinstalldistro_TRANSLATIONS_TARGET) + "/" + app.getApplicationName() + "_" + locale.bcp47Name() + "\n"));
    if (! translator.load(app.getApplicationName() + "_" + locale.bcp47Name())) {
        if (translator.load(QString(qinstalldistro_TRANSLATIONS_TARGET) + "/" + app.getApplicationName() + "_" + locale.bcp47Name())) {
            app.installTranslator(&translator);
            fprintf(stderr, "Loaded!\n");
        }
    }
    else {
        app.installTranslator(&translator);
        fprintf(stderr, "Loaded!!\n");
    }
    app.init();
    ret = app.exec();
    return ret;
}

void signalHandler(int signal)
{
    //print received signal
    switch(signal){
        case SIGINT:
        case SIGKILL:
        case SIGQUIT:
        case SIGSTOP:
        case SIGTERM:
        case SIGSEGV:
            break;
        default:
            break;
    }
    
    //app ends as expected
    fprintf(stderr, "Thus ends!!\n");
    QApplication::quit();
}
