#include "parted.h"
#include "mydebug.h"

Parted::Parted():
    m_devices(QMap<QString, PedDevice*>()),
    m_partitions(QMap<PedDisk*, QMap<PedPartition*, QString> >())
{
    getDevices();
    getPartitions();
}

Parted::~Parted()
{}

QStringList Parted::getListNamePartitions()
{
    QStringList listNamePartitions;
    if (!m_partitions.isEmpty()) {
        QList< QMap<PedPartition*, QString> > partitions = m_partitions.values();
        for (int i = 0; i < partitions.size(); i++) {
            listNamePartitions << partitions[i].values();
            listNamePartitions.sort();
#ifdef __DEBUG__
            MyDBG << listNamePartitions;
#endif
        }
    }
    return listNamePartitions;
}

QStringList Parted::getListNameDevices()
{
    QStringList listNameDevices;
    listNameDevices = m_devices.keys();
    listNameDevices.sort();
    return listNameDevices;
}

void Parted::getDevices()
{
    PedDevice* device = NULL;
    ped_device_probe_all();
    while ((device = ped_device_get_next(device)))
        m_devices.insert(QString(device->path), device);
}

void Parted::getPartitions()
{
    if (m_devices.isEmpty())
        return;
    QList<PedDevice*> devices = m_devices.values();
    for (int i = 0; i < devices.size(); i++) {
#ifdef __DEBUG__
        MyDBG << i << devices[i];
#endif
        PedDisk* disk = ped_disk_new(devices[i]);
#ifdef __DEBUG__
        MyDBG << disk;
#endif
        if (disk) {
            PedPartition* partition;
            QMap<PedPartition*, QString> partitions;
            for(partition = ped_disk_next_partition(disk, NULL); partition; partition = ped_disk_next_partition(disk, partition)) {
                if (partition) {
                    if (partition->num < 0)
                        continue;
                    if (partition->fs_type) {
                        partitions.insert(partition, QString(ped_partition_get_path(partition)));
                        MyDBG << ped_partition_get_path(partition);
                    }
                }
            }
            m_partitions.insert(disk, partitions);
        }
    }
}
