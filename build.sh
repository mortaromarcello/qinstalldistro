#!/usr/bin/env bash
REQUIRED_LIB="rsync cmake make g++ qtbase5-dev libparted0-dev qttools5-dev libboost-filesystem-dev libblkid-dev libmount-dev"
DATE=$(date +%Y%m%d%H%M)
ARCH=$(dpkg --print-architecture)
DEBIAN_REVISION=1
if [ -n "$1" ]; then
CONFIG_DIR=$1/.config
else
	CONFIG_DIR="${HOME}/.config"
fi
echo -n "${DATE}" > date.txt
echo -n "${ARCH}" > architecture.txt
if [ -d build ]; then
	rm -vfr build
fi
mkdir build
cd build
sudo apt-get update && sudo apt-get -y install ${REQUIRED_LIB}
cmake ../
make package
#sudo apt-get -y purge qinstalldistro
#sudo dpkg -i qinstalldistro_${DATE}-${DEBIAN_REVISION}_${ARCH}.deb
cd ..
#rm -R -f build
