// The configured options and settings for qinstalldistro

#define qinstalldistro_TRANSLATIONS_TARGET	"/usr/share/qinstalldistro/translations"
#define qinstalldistro_IMAGES_TARGET		"/usr/share/qinstalldistro/images"
#define qinstalldistro_SOUNDS_TARGET		"/usr/share/qinstalldistro"
#define qinstalldistro_ORGANIZATION			"mortaro"
#define qinstalldistro_APPLICATION			"qinstalldistro"
#define qinstalldistro_APPLETS_FILE_CONFIG	""
