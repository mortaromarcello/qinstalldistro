#ifndef QINSTALLDISTRO_H
#define QINSTALLDISTRO_H

#include <QApplication>
#include <QMainWindow>
#include <QProgressBar>
#include <QFont>
#include <QFile>
#include <QDir>
#include <QThread>
#include <unistd.h>

inline bool isUserRoot() {return (getuid()) ? false : true;}
inline const char* qstr2str(QString str) {return str.toLocal8Bit().data();}

class Parted;
class Ui_QInstallDistro;
class ScrollMessageBox;

class QInstallDistro: public QApplication
{
    Q_OBJECT
public:
    QInstallDistro(int& argc, char** argv);
    ~QInstallDistro();
    // ritorna la istanza di QInstallDistro (m_instance è settato a this oppure a NULL)
    static QInstallDistro* instance() {return m_instance;}
    void init();
    const QString getApplicationName() const {return applicationName();}
    int countFiles(const QString &path);
    bool copyPath(QString source, QString destination);
    QString findStringInFile(const QString &namefile, const QString &str);
    void replaceStringInFile(const QString &namefile, const QString &str, const QString &strToReplace);
    void messageInfo(const QString &message);
    QByteArray cryptPassword(const QString &password);
    const QStringList getListUsers(QString path);

public slots:

private slots:
    void goInstall();
    void abort();
    bool createRootAndMountPartition();
    bool createHomeAndMountPartition();
    bool createSwapPartition();
    bool copyRoot();
    bool addUser();
    bool addSudoUser();
    bool changeRootPassword();
    bool setAutologin();
    bool createFstab();
    bool setLocale();
    bool setTimezone();
    bool setHostname();
    bool installGrub();
    bool upgradeSystem();

private:
    // functions
    bool checkValues();
    const QByteArray processCommandSync(QString command, int & exitCode);
    //
    QString m_checkError;
    //
    QStringList m_dirsToCopy;
    QStringList m_dirsToCreate;
    QString m_distro;
    QString m_rootPartition;
    QString m_homePartition;
    QString m_swapPartition;
    bool m_formatHome;
    QString m_uuidRootPartition;
    QString m_uuidHomePartition;
    QString m_uuidSwapPartition;
    QString m_instDrive;
    QDir m_instRootDirectory;
    QString m_typeFS;
    QString m_user;
    QByteArray m_cryptUserPassword;
    QByteArray m_cryptRootPassword;
    bool m_autologin;
    QString m_locale;
    QString m_lang;
    QString m_keyboard;
    QString m_hostname;
    QString m_groups;
    QString m_timezone;
    QString m_shellUser;
    QDir m_squashFS;
    QFile m_squashFile;
    bool m_useHome;
    bool m_upgrade;
    bool m_nopasswd;
    bool m_optimizeDiskUsb;
    // !!! parted!!!
    Parted* m_parted;
    //
    static QInstallDistro* m_instance;
    static int m_numFilesCopied;
    static int m_numFilesToCopy;
    QMainWindow* m_mainWindow;
    QStatusBar* m_statusBar;
    QProgressBar* m_progressBar;
    Ui_QInstallDistro* m_ui;
    QAction* m_actionExit;
};

#endif
