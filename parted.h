#ifndef PARTED_H
#define PARTED_H

#include <QObject>
#include <QMap>
#include <QStringList>
#include <parted/parted.h>

class Parted: public QObject
{
    Q_OBJECT
public:
    Parted();
    ~Parted();
    QMap<QString, PedDevice*> devices() {return m_devices;}
    QMap<PedDisk*, QMap<PedPartition*, QString> > partitions() {return m_partitions;}
    QStringList getListNamePartitions();
    QStringList getListNameDevices();
private:
    void getDevices();
    void getPartitions();
    QMap<QString, PedDevice*> m_devices;
    QMap<PedDisk*, QMap<PedPartition*, QString> > m_partitions;
};

#endif // PARTED_H
