#include "config.h"
#include "qinstalldistro.h"
#include "mydebug.h"
#include "parted.h"
#include "scrollmessagebox.h"
#include <blkid/blkid.h>
#include <sys/mount.h>
#include <QSettings>
#include <QTimer>
#include <QtDebug>
#include <QDir>
#include <QColorDialog>
#include <QStyleFactory>
#include <QProcess>
#include <QCryptographicHash>
#include <boost/filesystem.hpp>
#include <iostream>
#include "ui_qinstalldistro.h"

boost::filesystem::path PathFromQString(const QString & filePath)
{
#ifdef _WIN32
    auto * wptr = reinterpret_cast<const wchar_t*>(filePath.utf16());
    return boost::filesystem::path(wptr, wptr + filePath.size());
#else
    return boost::filesystem::path(filePath.toStdString());
#endif
}

QString QStringFromPath(const boost::filesystem::path & filePath)
{
#ifdef _WIN32
    return QString::fromStdWString(filePath.generic_wstring());
#else
    return QString::fromStdString(filePath.native());
#endif
}

QString blkid(const QString& device)
{
    blkid_probe pr;
    const char* uuid;
    pr = blkid_new_probe_from_filename(device.toLatin1());
    if (!pr)
        return QString();
    blkid_do_probe(pr);
    blkid_probe_lookup_value(pr, "UUID", &uuid, NULL);
    return QString(uuid);
}

// random string
QByteArray stringRandom(int len)
{
    QByteArray res;
    const char alphanum[] =
    "0123456789"
    "!@#$%^&*"
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz";
    int stringLength = sizeof(alphanum) - 1;
    for (int i = 0; i < len; i++) {
        res += alphanum[rand() % stringLength];
    }
    return res;
}

// setta m_instance a NULL
QInstallDistro* QInstallDistro::m_instance = NULL;
int QInstallDistro::m_numFilesCopied = 0;
int QInstallDistro::m_numFilesToCopy = 0;

QInstallDistro::QInstallDistro(int& argc, char** argv)
    : QApplication(argc, argv),
    m_mainWindow(new QMainWindow)
{
    m_instance = this;
    m_ui = new Ui::QInstallDistro();
    m_ui->setupUi(m_mainWindow);
    setOrganizationName(qinstalldistro_ORGANIZATION);
    setApplicationName(qinstalldistro_APPLICATION);
    if (!QIcon::themeSearchPaths().contains("/usr/share/pixmaps"))
        QIcon::setThemeSearchPaths(QIcon::themeSearchPaths() << "/usr/share/pixmaps");
    QSettings settings;
}

QInstallDistro::~QInstallDistro()
{
    m_instance = NULL;
    m_mainWindow = NULL;
}

void QInstallDistro::init()
{
    m_dirsToCopy<<"bin"<<"boot"<<"etc"<<"lib"<<"opt"<<"root"<<"run"<<"sbin"<<"srv"<<"usr"<<"var";
    m_dirsToCreate<<"home"<<"media"<<"mnt"<<"proc"<<"selinux"<<"sys"<<"tmp";
    m_distro = "devuan";
    m_formatHome = false;
    m_instRootDirectory = QDir("/mnt/" + m_distro);
    m_typeFS = "ext4";
    m_autologin = true;
    m_locale = "it_IT.UTF-8 UTF-8";
    m_lang = "it_IT.UTF-8";
    m_keyboard = "it";
    m_hostname = m_distro;
    m_groups = "cdrom,floppy,audio,dip,video,plugdev,lp,dialout,netdev";
    m_timezone = "Europe/Rome";
    m_shellUser = "/bin/bash";
    m_squashFS = QDir("/lib/live/mount/rootfs/filesystem.squashfs");
    m_squashFile.setFileName("/lib/live/mount/medium/live/filesystem.squashfs");
    m_useHome = false;
    m_nopasswd = true;
    m_optimizeDiskUsb = false;
    // !!! parted!!!
    m_parted = new Parted();
    //
    m_ui->comboBoxRootPartition->addItems(m_parted->getListNamePartitions());
    m_ui->comboBoxInstallationDrive->addItems(m_parted->getListNameDevices());
    m_ui->comboBoxHomePartition->addItems(m_parted->getListNamePartitions());
    m_ui->comboBoxHomePartition->setEnabled(m_useHome);
    m_ui->checkBoxUseHomePartition->setChecked(m_useHome);
    m_ui->checkBoxNoPasswd->setChecked(m_nopasswd);
    m_ui->checkBoxAutoLogin->setChecked(m_autologin);
    m_ui->comboBoxLocale->addItems(QStringList() << "it_IT.UTF-8 UTF-8" << "us_US.UTF-8 UTF-8" << "es_ES.UTF-8 UTF-8" << "de_DE.UTF-8 UTF-8" << "fr_FR.UTF-8 UTF-8");
    m_ui->comboBoxLanguage->addItems(QStringList() << "it_IT.UTF-8" << "us_US.UTF-8" << "es_ES.UTF-8" << "de_DE.UTF-8" << "fr_FR.UTF-8");
    m_ui->comboBoxKeyboard->addItems(QStringList() << "it" << "us" << "es" << "de" << "fr");
    m_ui->lineEditHostname->setText(m_hostname);
    m_ui->lineEditGroups->setText(m_groups);
    m_ui->comboBoxTimezone->addItems(QStringList() << "Europe/Rome" << "Europe/London" << "Europe/Madrid" << "Europe/Berlin" << "Europe/Paris" << "Etc/UTC");
    m_ui->lineEditShell->setText(m_shellUser);
    //
    m_ui->textEditCommandOutput->setText(tr("QinstallDistro"));
    //
    connect(m_ui->action_Exit, SIGNAL(triggered()), this, SLOT(quit()));
    connect(m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(goInstall()));
    connect(m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(abort()));
    //
    m_ui->retranslateUi(m_mainWindow);
    m_statusBar = m_mainWindow->statusBar();
    m_progressBar = new QProgressBar();
    m_statusBar->showMessage(QString("%1 - %2").arg(qinstalldistro_ORGANIZATION).arg(qinstalldistro_APPLICATION));
    m_mainWindow->show();
}

void QInstallDistro::goInstall()
{
    int exitCode;
    bool isOk = true;
    QByteArray salt;
    QByteArray output;
#ifdef __DEBUG__
    MyDBG << "goInstall()";
#endif
    m_statusBar->showMessage("goInstall");
    isOk = checkValues();
    //
    if (! isOk) {
#ifdef __DEBUG__
        MyDBG << tr("Values checked not are correct.");
#endif
        return;
    }
    else {
        // messagebox
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(nullptr, tr("Question"), tr("The check is passed.\nroot partition=%1\nno password for sudo=%2\nuse home=%3\nauto login=%4\nhome partition=%5\ndrive to install=%6\nuser name=%7\npassword user crypt=%8\nroot user crypt=%9\nlocale=%10\nlanguage=%11\nkeyboard=%12\nhostname=%13\ngroups=%14\ntimezone=%15\nshell=%16\n\nTO CONTINUE?").arg(m_rootPartition).arg(m_nopasswd).arg(m_useHome).arg(m_autologin).arg(m_homePartition).arg(m_instDrive).arg(m_user).arg(m_cryptUserPassword.data()).arg(m_cryptRootPassword.data()).arg(m_locale).arg(m_lang).arg(m_keyboard).arg(m_hostname).arg(m_groups).arg(m_timezone).arg(m_shellUser), QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::No) {
            return;
        }
    }
    isOk = createRootAndMountPartition();
    if (isOk) {
        if (m_useHome)
            isOk = createHomeAndMountPartition();
    }
    if (isOk) {
        isOk = createSwapPartition();
    }
    if (isOk) {
        isOk = copyRoot();
    }
    if (isOk) {
        isOk = addUser();
    }
    if (isOk) {
        isOk = changeRootPassword();
    }
    if (isOk) {
        isOk = addSudoUser();
    }
    if (isOk) {
        isOk = setAutologin();
    }
    if (isOk) {
        isOk = createFstab();
    }
    if (isOk) {
        isOk = setLocale();
    }
    if (isOk) {
        isOk = setTimezone();
    }
    if (isOk) {
        isOk = setHostname();
    }
    if (isOk) {
        isOk = installGrub();
    }
    if (isOk) {
        if (m_upgrade)
            isOk = upgradeSystem();
    }
    if (isOk) {
        QCoreApplication::processEvents();
        messageInfo("Fatto!");
        QCoreApplication::quit();
    }
}

void QInstallDistro::abort()
{
    MyDBG << "abort()";
    QCoreApplication::exit();
}

bool QInstallDistro::createRootAndMountPartition()
{
    int exitCode;
    QByteArray output;
    MyDBG << "createRootAndMountPartition()";
    m_statusBar->showMessage(tr("Create and mount root partition"));
    if (! m_rootPartition.isEmpty()) {
        QCoreApplication::processEvents();
        output = processCommandSync(QString("mkfs -t %1 %2").arg(m_typeFS).arg(m_rootPartition), exitCode);
        m_ui->textEditCommandOutput->append(output);
        if (exitCode) {
            messageInfo(QString(tr("mkfs command returned error code %1.\n")).arg(exitCode));
            return false;
        }
        m_uuidRootPartition = blkid(m_rootPartition);
        output = processCommandSync(QString("mkdir -p %1").arg(m_instRootDirectory.absolutePath()), exitCode);
        m_ui->textEditCommandOutput->append(output);
        if (exitCode) {
            messageInfo(QString(tr("mkdir command returned error code %1.\n")).arg(exitCode));
            return false;
        }
        output = processCommandSync(QString("mount %1 %2").arg(m_rootPartition).arg(m_instRootDirectory.absolutePath()), exitCode);
        m_ui->textEditCommandOutput->append(output);
        if (exitCode) {
            messageInfo(QString(tr("mount command returned error code %1.\n")).arg(exitCode));
            return false;
        }
    }
    return true;
}

bool QInstallDistro::createHomeAndMountPartition()
{
    int exitCode;
    QByteArray output;
    MyDBG << "createHomeAndMountPartition()";
    m_statusBar->showMessage(tr("Create Home and mount partition"));
    if (! m_homePartition.isEmpty()) {
        if (m_formatHome) {
            QCoreApplication::processEvents();
            output = processCommandSync(QString("mkfs -t %1 %2").arg(m_typeFS).arg(m_homePartition), exitCode);
            m_ui->textEditCommandOutput->append(output);
            if (exitCode) {
                messageInfo(QString(tr("mkfs command returned error code %1.\n")).arg(exitCode));
                return false;
            }
        }
        m_uuidHomePartition = blkid(m_homePartition);
        output = processCommandSync(QString("mkdir -p %1/home").arg(m_instRootDirectory.absolutePath()), exitCode);
        m_ui->textEditCommandOutput->append(output);
        if (exitCode) {
            messageInfo(QString(tr("mkdir command returned error code %1.\n")).arg(exitCode));
            return false;
        }
        output = processCommandSync(QString("mount %1 %2/home").arg(m_homePartition).arg(m_instRootDirectory.absolutePath()), exitCode);
        m_ui->textEditCommandOutput->append(output);
        if (exitCode) {
            messageInfo(QString(tr("mount command returned error code %1.\n")).arg(exitCode));
            return false;
        }
    }
    return true;
}

bool QInstallDistro::createSwapPartition()
{
    int exitCode;
    QByteArray output;
    if (! m_swapPartition.isEmpty()) {
        MyDBG << "createSwapPartition()";
        m_statusBar->showMessage(tr("Create swap partition"));
        output = processCommandSync(QString("mkswap -c %1").arg(m_swapPartition), exitCode);
        m_ui->textEditCommandOutput->append(output);
        if (exitCode) {
            messageInfo(QString(tr("mkswap command returned error code %1.\n")).arg(exitCode));
            return false;
        }
    }
    return true;
}

bool QInstallDistro::copyRoot()
{
    MyDBG << "copyRoot()";
    //int n_files_for_copy;
    m_statusBar->showMessage(tr("copy root"));
    m_ui->textEditCommandOutput->append(tr("Copy root"));
    //m_numFilesToCopy = countFiles(m_squashFS.absolutePath());
    m_ui->textEditCommandOutput->append(QString("%1").arg(m_numFilesToCopy));
    //m_statusBar->addWidget(m_progressBar);
    //m_progressBar->setRange(0,100);
    //m_myThread.start();
    //while (! m_myThread.isFinished()) {
    //    QCoreApplication::processEvents();
    //}
    copyPath(m_squashFS.absolutePath(), m_instRootDirectory.absolutePath());
    return true;
}

bool QInstallDistro::addUser()
{
    MyDBG << "addUser()";
    QByteArray output;
    int exitCode;
    output = processCommandSync(QString("chroot %1 bash -c \"useradd -G %2 -s %3 -m -p %4 %5\"").arg(m_instRootDirectory.absolutePath()).arg(m_groups).arg(m_shellUser).arg(m_cryptUserPassword.data()).arg(m_user), exitCode);
    m_ui->textEditCommandOutput->append(output);
    if (exitCode) {
        messageInfo(QString(tr("useradd command returned error code %1.\n")).arg(exitCode));
        return false;
    }
    return true;
}

bool QInstallDistro::addSudoUser()
{
    int exitCode;
    QByteArray output;
    std::cerr << "addSudoUser()" << '\n';
    m_statusBar->showMessage(tr("Add the user to the sudo group"));
    output = processCommandSync(QString("chroot %1 gpasswd -a %2 sudo").arg(m_instRootDirectory.absolutePath()).arg(m_user), exitCode);
    m_ui->textEditCommandOutput->append(output);
    if (exitCode)
    {
        messageInfo(QString("The command gpasswd has returned error code %1").arg(exitCode));
        return false;
    }
    if (m_nopasswd)
    {
        QFile f(QString("%1/etc/sudoers.d/nopasswd").arg(m_instRootDirectory.absolutePath()));
        f.open(QIODevice::WriteOnly);
        f.write(QString("%1 ALL=(ALL) NOPASSWD: ALL\n").arg(m_user).toUtf8());
        f.close();
    }
    return true;

}

bool QInstallDistro::changeRootPassword()
{
    int exitCode;
    QByteArray output;
    MyDBG << "changeRootPassword()";
    m_statusBar->showMessage(tr("Change the root password"));
    output = processCommandSync(QString("chroot %1 bash -c \"echo 'root:%2' | chpasswd -e\"").arg(m_instRootDirectory.absolutePath()).arg(m_cryptRootPassword.data()), exitCode);
    if (exitCode)
    {
        messageInfo(QString(tr("useradd command returned error code %1.\n")).arg(exitCode));
        return false;
    }
    return true;
}

bool QInstallDistro::setAutologin()
{
    MyDBG << "setAutologin()";
    if (m_autologin) {
        QFile file(m_instRootDirectory.absolutePath() + QString("/etc/X11/default-display-manager"));
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QTextStream in(&file);
        QString line = in.readAll();
        MyDBG << line;
        if (line.isEmpty()) {
            messageInfo("Default display-manager not found.");
            return false;
        }
        QRegExp regExp;
        if (line.contains("/usr/bin/slim")) {
            QString str = findStringInFile(QString("%1/etc/slim.conf").arg(m_instRootDirectory.absolutePath()), "#default_user");
            if (! str.isEmpty()) {
                replaceStringInFile(QString("%1/etc/slim.conf").arg(m_instRootDirectory.absolutePath()), str, QString("default_user          %1\n").arg(m_user));
            }
        }
        else if (line.contains("/usr/sbin/gdm3")) {
            QString str = findStringInFile(QString("%1/etc/gdm3/daemon.conf").arg(m_instRootDirectory.absolutePath()), "AutomaticLogin ");
            if (! str.isEmpty()) {
                replaceStringInFile(QString("%1/etc/gdm3/daemon.conf").arg(m_instRootDirectory.absolutePath()), str, QString("AutomaticLogin = %1\n").arg(m_user));
            }
        }
        else if (line.contains("/usr/bin/lightdm")) {
            QString str = findStringInFile(QString("%1/etc/lightdm/lightdm.conf").arg(m_instRootDirectory.absolutePath()), "#autologin-user=");
            if (! str.isEmpty()) {
                replaceStringInFile(QString("%1/etc/lightdm/lightdm.conf").arg(m_instRootDirectory.absolutePath()), str, QString("autologin-user=%1\n").arg(m_user));
                str = findStringInFile(QString("%1/etc/lightdm/lightdm.conf").arg(m_instRootDirectory.absolutePath()), "#autologin-user-timeout=0");
                if (! str.isEmpty()) {
                    replaceStringInFile(QString("%1/etc/lightdm/lightdm.conf").arg(m_instRootDirectory.absolutePath()), str, QString("autologin-user-timeout=0\n"));
                }
            }
        }
    }
    return true;
}

bool QInstallDistro::createFstab()
{
    QString buff;
    MyDBG << "createFstab()";
    if (m_optimizeDiskUsb)
        buff = QString("# /etc/fstab: static file system information.\n#\n# Use 'blkid' to print the universally unique identifier for a\n# device; this may be used with UUID= as a more robust way to name devices\n# that works even if disks are added and removed. See fstab(5).\n#\n# <file system> <mount point> <type> <options> <dump> <pass>\nproc /proc proc defaults 0 0\nUUID=%1 / %2 noatime,nodiratime,errors=remount-ro 0 1\n").arg(m_uuidRootPartition).arg(m_typeFS);
    else
        buff = QString("# /etc/fstab: static file system information.\n#\n# Use 'blkid' to print the universally unique identifier for a\n# device; this may be used with UUID= as a more robust way to name devices\n# that works even if disks are added and removed. See fstab(5).\n#\n# <file system> <mount point> <type> <options> <dump> <pass>\nproc /proc proc defaults 0 0\nUUID=%1 / %2 errors=remount-ro 0 1\n").arg(m_uuidRootPartition).arg(m_typeFS);
    if (! m_uuidHomePartition.isEmpty()) {
        if (m_optimizeDiskUsb) {
            buff = buff + QString("UUID=%1 /home %2 noatime,nodiratime,defaults 0 2\n").arg(m_uuidHomePartition).arg(m_typeFS);
        }
        else {
            buff = buff + QString("UUID=%1 /home %2 defaults 0 2\n").arg(m_uuidHomePartition).arg(m_typeFS);
        }
    }
    if (! m_uuidSwapPartition.isEmpty()) {
        buff = buff + QString("UUID=%1 none swap sw 0 0\n").arg(m_uuidSwapPartition);
    }
    QFile file(QString("%1/etc/fstab").arg(m_instRootDirectory.absolutePath()));
    file.open(QIODevice::WriteOnly);
    file.write(buff.toLocal8Bit());
    return true;
}

bool QInstallDistro::setLocale()
{
    int exitCode;
    QByteArray output;
    MyDBG << "setLocale()";
    QString line = findStringInFile(QString("%1/etc/locale.gen").arg(m_instRootDirectory.absolutePath()), m_locale);
    if (! line.isEmpty()) {
        replaceStringInFile(QString("%1/etc/locale.gen").arg(m_instRootDirectory.absolutePath()), line, m_locale); 
    }
    line = findStringInFile(QString("%1/etc/default/keyboard").arg(m_instRootDirectory.absolutePath()), "XKBLAYOUT");
    if (! line.isEmpty()) {
        replaceStringInFile(QString("%1/etc/default/keyboard").arg(m_instRootDirectory.absolutePath()), line, QString("XKBLAYOUT=\"%1\"\n").arg(m_keyboard));
    }
    output = processCommandSync(QString("chroot %1 bash -c \"locale-gen; if [ \"$0\" != \"0\" ]; then update-locale LANG=%2;fi\"").arg(m_instRootDirectory.absolutePath()).arg(m_lang), exitCode);
    m_ui->textEditCommandOutput->append(output);
    if (exitCode) {
            messageInfo(QString(tr("locale commands returned error code %1.\n")).arg(exitCode));
            return false;
        }
    return true;
}

bool QInstallDistro::setTimezone()
{
    MyDBG << "setTimezone()";
    QFile file(QString("%1/etc/timezone").arg(m_instRootDirectory.absolutePath()));
    file.open(QIODevice::WriteOnly);
    file.write(QString("%1\n").arg(m_timezone).toLocal8Bit());
    file.close();
    QFile::copy(QString("%1/usr/share/zoneinfo/%2").arg(m_instRootDirectory.absolutePath()).arg(m_timezone), QString("%1/etc/localtime").arg(m_instRootDirectory.absolutePath()));
    return true;
}

bool QInstallDistro::setHostname()
{
    MyDBG << "setHostname()";
    QFile file(QString("%1/etc/hostname").arg(m_instRootDirectory.absolutePath()));
    file.open(QIODevice::WriteOnly);
    file.write(QString("%1\n").arg(m_hostname).toLocal8Bit());
    file.close();
    QFile file2(QString("%1/etc/hosts").arg(m_instRootDirectory.absolutePath()));
    file2.write(QString("127.0.0.1       localhost\n127.0.1.1       %1\n::1             localhost ip6-localhost ip6-loopback\nfe00::0         ip6-localnet\nff00::0         ip6-mcastprefix\nff02::1         ip6-allnodes\nff02::2         ip6-allrouters\n").arg(m_hostname).toLocal8Bit());
    file2.close();
    return true;
}

bool QInstallDistro::installGrub()
{
    MyDBG << "installGrub()";
    int exitCode;
    QByteArray output;
    m_statusBar->showMessage(tr("Install grub"));
    QStringList dirs;
    dirs << "dev" << "sys" << "proc";
    for (int i = 0; i < dirs.size(); i++) {
        output = processCommandSync(QString("mount -B /%1 %2/%3").arg(dirs.at(i)).arg(m_instRootDirectory.absolutePath()).arg(dirs.at(i)), exitCode);
        m_ui->textEditCommandOutput->append(output);
    }
    if (exitCode) {
        // mod
        
        messageInfo(QString(tr("mount command returned error code %1.\n")).arg(exitCode));
        for (int i = 0; i < dirs.size(); i++) {
            output = processCommandSync(QString("umount %1/%2").arg(m_instRootDirectory.absolutePath()).arg(dirs.at(i)), exitCode);
            m_ui->textEditCommandOutput->append(output);
            if (exitCode) {
                messageInfo(QString(tr("umount command returned error code %1.\n")).arg(exitCode));
                return false;
            }
        }
        return false;
    }
    output = processCommandSync(QString("chroot %1 bash -c \"grub-install --no-floppy %2\"").arg(m_instRootDirectory.absolutePath()).arg(m_instDrive), exitCode);
    m_ui->textEditCommandOutput->append(output);
    if (exitCode) {
        messageInfo(QString(tr("grub-install command returned error code %1.\n")).arg(exitCode));
        for (int i = 0; i < dirs.size(); i++) {
            output = processCommandSync(QString("umount %1/%2").arg(m_instRootDirectory.absolutePath()).arg(dirs.at(i)), exitCode);
            m_ui->textEditCommandOutput->append(output);
            if (exitCode) {
                messageInfo(QString(tr("umount command returned error code %1.\n")).arg(exitCode));
                return false;
            }
        }
        return false;
    }
    output = processCommandSync(QString("chroot %1 bash -c \" update-grub\"").arg(m_instRootDirectory.absolutePath()), exitCode);
    m_ui->textEditCommandOutput->append(output);
    if (exitCode) {
        messageInfo(QString(tr("update-grub command returned error code %1.\n")).arg(exitCode));
        for (int i = 0; i < dirs.size(); i++) {
            output = processCommandSync(QString("umount %1/%2").arg(m_instRootDirectory.absolutePath()).arg(dirs.at(i)), exitCode);
            m_ui->textEditCommandOutput->append(output);
            if (exitCode) {
                messageInfo(QString(tr("umount command returned error code %1.\n")).arg(exitCode));
                return false;
            }
        }
        return false;
    }
    return true;
}

bool QInstallDistro::upgradeSystem()
{
    int exitCode;
    QByteArray output;
    MyDBG << "upgradeSystem()";
    m_statusBar->showMessage(tr("Upgrade system"));
    output = processCommandSync(QString("chroot %1 bash -c \"apt-get update && apt-get --yes -V dist-upgrade\"").arg(m_instRootDirectory.absolutePath()), exitCode);
    if (exitCode) {
        messageInfo(QString(tr("apt-get command returned error code %1.\n")).arg(exitCode));
        return false;
    }
    return true;
}

// private functions
bool QInstallDistro::checkValues()
{
    bool ret = true;
    QStringList errors;
    m_rootPartition = m_ui->comboBoxRootPartition->currentText();
    if (m_rootPartition.isEmpty()) {
        m_checkError = tr("Root partition is empty");
        errors << m_checkError;
#ifdef __DEBUG__
        MyDBG << m_checkError;
#endif
        ret = false;
    }
#ifdef __DEBUG__
    MyDBG << m_rootPartition;
#endif
    m_useHome = m_ui->checkBoxUseHomePartition->isChecked();
    m_nopasswd = m_ui->checkBoxNoPasswd->isChecked();
    m_autologin = m_ui->checkBoxAutoLogin->isChecked();
    if (m_useHome) {
        m_homePartition = m_ui->comboBoxHomePartition->currentText();
    }
    if (!m_homePartition.isEmpty()) {
        if (m_rootPartition == m_homePartition) {
            m_checkError = "Root partition = home partition";
            errors << m_checkError;
#ifdef __DEBUG__
            MyDBG << m_checkError;
#endif
            ret = false;
        }
    }
    if (m_homePartition.isEmpty() && m_useHome) {
        m_checkError = "use home is set but home partition is empty";
            errors << m_checkError;
#ifdef __DEBUG__
            MyDBG << m_checkError;
#endif
            ret = false;
    }
    m_instDrive = m_ui->comboBoxInstallationDrive->currentText();
    if (m_instDrive.isEmpty()) {
        m_checkError = tr("Installation drive is empty");
        errors << m_checkError;
#ifdef __DEBUG__
        MyDBG << m_checkError;
#endif
        ret = false;
    }
    m_user = m_ui->lineEditUsername->text();
    if (m_user.isEmpty())
    {
        m_checkError = tr("Username is empty");
        errors << m_checkError;
#ifdef __DEBUG__
        MyDBG << m_checkError;
#endif
        ret = false;
    }
    foreach(QString user, getListUsers(m_squashFS.absolutePath()))
    {
        if (m_user.compare(user) == 0)
        {
            m_checkError = tr("Username not valid. Already exist.");
            errors << m_checkError;
#ifdef __DEBUG__
            MyDBG << m_checkError;
#endif
            ret = false;
        }
    }
    m_locale = m_ui->comboBoxLocale->currentText();
    m_lang = m_ui->comboBoxLanguage->currentText();
    m_keyboard = m_ui->comboBoxKeyboard->currentText();
    m_hostname = m_ui->lineEditHostname->text();
    m_groups = m_ui->lineEditGroups->text();
    m_timezone = m_ui->comboBoxTimezone->currentText();
    m_shellUser = m_ui->lineEditShell->text();
    if (! m_ui->lineEditPassword->text().isEmpty())
        m_cryptUserPassword = cryptPassword(m_ui->lineEditPassword->text());
    else {
        m_checkError = tr("User password is empty");
        errors << m_checkError;
#ifdef __DEBUG__
        MyDBG << m_checkError;
#endif
        ret = false;
    }
    if (! m_ui->lineEditRootPassword->text().isEmpty())
        m_cryptRootPassword = cryptPassword(m_ui->lineEditRootPassword->text());
    else {
        m_checkError = tr("Root password is empty");
        errors << m_checkError;
#ifdef __DEBUG__
        MyDBG << m_checkError;
#endif
        ret = false;
    }
    //
    if (m_locale.isEmpty()) {
        m_checkError = tr("Locale is empty");
        errors << m_checkError;
#ifdef __DEBUG__
        MyDBG << m_checkError;
#endif
        ret = false;
    }
    if (m_hostname.isEmpty()) {
        m_checkError = tr("Hostname is empty");
        errors << m_checkError;
#ifdef __DEBUG__
        MyDBG << m_checkError;
#endif
        ret = false;
    }
    if (m_groups.isEmpty()) {
        m_checkError = tr("Groups is empty");
        errors << m_checkError;
#ifdef __DEBUG__
        MyDBG << m_checkError;
#endif
        ret = false;
    }
    if (! ret) {
        messageInfo(QString("Values checked not are correct.\n%1").arg(errors.join("\n")));
    }
    return ret;
}
bool QInstallDistro::copyPath(QString source, QString destination)
{
    using namespace boost::filesystem;
    using namespace std;
    path const & src = PathFromQString(source);
    path const & dst = PathFromQString(destination);
    file_status s = status(src);
    if (is_symlink(src))
    {
        copy_symlink(src, dst);
        std::cerr << src << "->" << dst << '\n';
        m_ui->textEditCommandOutput->append(QStringFromPath(src) + "->" + QStringFromPath(dst));
    }
    else if (is_directory(src))
    {
        if (!exists(dst))
        {
            copy_directory(src, dst);
            permissions(dst, s.permissions());
        }
        for (directory_iterator file(src); file != directory_iterator(); ++file)
        {
            QCoreApplication::processEvents();
            path current(file->path());
            s = status(current);
            if(is_symlink(current))
            {
                copy_symlink(current, dst/current.filename());
                std::cerr << current.filename() << "->" << dst/current.filename() << '\n';
                m_ui->textEditCommandOutput->append(QStringFromPath(current.filename()) + "->" + QStringFromPath(dst/current.filename()));
            }
            else if(is_directory(current))
            {
                // Found directory: Recursion
                if(!copyPath(QStringFromPath(current), QStringFromPath(dst/current.filename())))
                {
                    return false;
                }
            }
            else
            {
                // Found file: Copy
                if (is_regular_file(current))
                {
                    copy_file(current, dst/current.filename());
                    permissions(dst/current.filename(), s.permissions());
                    std::cerr << current.filename() << "->" << dst/current.filename() << '\n';
                    m_ui->textEditCommandOutput->append(QStringFromPath(current.filename()) + "->" + QStringFromPath(dst/current.filename()));
                }
            }
        }
    }
    else if (is_regular_file(src))
    {
        cerr << src << "->" << dst << '\n';
        copy_file(src, dst);
        permissions(dst, s.permissions());
    }
    else
    {
        throw runtime_error(dst.generic_string() + " not dir or file");
    }
    /*
    try
    {
        // Check whether the function call is valid
        if(
            !fs::exists(source) ||
            !fs::is_directory(source)
        )
        {
            std::cerr << "Source directory " << source.string()
                << " does not exist or is not a directory." << '\n'
            ;
            return false;
        }
        if(fs::exists(destination))
        {
            std::cerr << "Destination directory " << destination.string() << " already exists." << '\n';
            //return false;
        }
        // Create the destination directory
        else
        {
            if(!fs::create_directory(destination))
            {
                std::cerr << "Unable to create destination directory" << destination.string() << '\n';
                return false;
            }
            else
            {
                std::cerr << "mkdir " << destination.string() << '\n';
                m_ui->textEditCommandOutput->append(QString("mkdir ") + QStringFromPath(destination.string()));
            }
        }
    }
    catch(fs::filesystem_error const & e)
    {
        std::cerr << e.what() << '\n';
        return false;
    }
    // Iterate through the source directory
    for(
        fs::directory_iterator file(source);
        file != fs::directory_iterator(); ++file
    )
    {
        QCoreApplication::processEvents();
        try
        {
            fs::path current(file->path());
            if(fs::is_symlink(current))
            {
                fs::copy_symlink(current, destination / current.filename());
                std::cerr << current.filename() << "->" << destination /current.filename() << '\n';
                m_ui->textEditCommandOutput->append(QString() + QStringFromPath(current.filename()) + "->" + QStringFromPath(destination /current.filename()));
            }
            else if(fs::is_directory(current))
            {
                // Found directory: Recursion
                if(
                    !copyPath(
                        QStringFromPath(current),
                        QStringFromPath(destination / current.filename())
                    )
                )
                {
                    return false;
                }
            }
            else
            {
                // Found file: Copy
                if (fs::is_regular_file(current))
                fs::copy_file(
                    current,
                    destination / current.filename()
                );
                std::cerr << current.filename() << "->" << destination / current.filename() << '\n';
                m_ui->textEditCommandOutput->append(QString() + QStringFromPath(current.filename()) + "->" + QStringFromPath(destination / current.filename()));
            }
        }
        catch(fs::filesystem_error const & e)
        {
            std:: cerr << e.what() << '\n';
        }
    }
    */
    return true;
}

QString QInstallDistro::findStringInFile(const QString &namefile, const QString &str)
{
    QFile file(namefile);
    file.open(QIODevice::ReadOnly);
    QTextStream in (&file);
    QString line;
    do {
        line = in.readLine();
        if (line.contains(str))
            return line;
    } while(!line.isNull());
    return QString();
}

void QInstallDistro::replaceStringInFile(const QString &namefile, const QString &str, const QString &strToReplace)
{
    QByteArray fileData;
    QFile file(namefile);
    file.open(QIODevice::ReadWrite);
    fileData = file.readAll();
    QString text(fileData);
    text.replace(str, strToReplace);
    file.seek(0);
    file.write(text.toUtf8());
    file.close();
}

int QInstallDistro::countFiles(const QString &path)
{
    int count = 0;
    boost::filesystem::path const &source = PathFromQString(path);
    namespace fs = boost::filesystem;
    try
    {
        if(
            !fs::exists(source) ||
            !fs::is_directory(source)
        )
        {
            std::cerr << "Source directory " << source.string()
                << " does not exist or is not a directory." << '\n'
            ;
            return -1;
        }
    }
    catch(fs::filesystem_error const & e)
    {
        std:: cerr << e.what() << '\n';
    }
    // Iterate through the source directory
    for(fs::recursive_directory_iterator file(source);file != fs::recursive_directory_iterator(); ++file)
    {
        QCoreApplication::processEvents();
        try
        {
            fs::path current(file->path());
            if (fs::is_regular_file(current))
            {
                count++;
                std::cerr << count << ":" << current << '\n';
            }
        }
        catch(fs::filesystem_error const & e)
        {
            std:: cerr << e.what() << '\n';
        }
    }
    return count;
}

const QByteArray QInstallDistro::processCommandSync(QString command, int & exitCode)
{
    QProcess proc;
    QByteArray output;
    proc.start(command);
    while (proc.waitForReadyRead()) {
        output += proc.readAll();
    }
    exitCode = proc.exitCode();
    return output;
}

void QInstallDistro::messageInfo(const QString &message)
{
    QMessageBox::information(nullptr, tr("Information"), message);
}

QByteArray QInstallDistro::cryptPassword(const QString &password)
{
    QByteArray output;
    int exitCode;
    QString command = QString("bash -c \"perl -e 'print crypt($ARGV[0],\"password\")' %1").arg(password);
    output = processCommandSync(command, exitCode);
    return output;
}

const QStringList QInstallDistro::getListUsers(QString path)
{
    int exitCode;
    QByteArray output;
    if (path.isEmpty())
        output = processCommandSync(QString("bash -c \"compgen -u\""), exitCode);
    else
        output = processCommandSync(QString("chroot %1 bash -c \"compgen -u\"").arg(path), exitCode);
    QStringList list = QString(output).split('\n');
    return list;
}
