function create_imagefile() {
	dd if=/dev/null of=persistence bs=1 count=0 seek=1G
	mkfs.ext4 -F persistence
	mount -t ext4 persistence /mnt
	echo -e "/home\n/var/cache/apt\n/etc\n" > /mnt/persistence.conf
	umount /mnt
}
