#include "../qinstalldistro.h"
#include <QDir>
#include <iostream>
#include <QDebug>

void test_countFiles(QInstallDistro *test)
{
    std::cerr << qstr2str(QString("test countFiles: %1\n").arg(test->countFiles("../"))) << '\n';
}

void test_copyPath(QInstallDistro *test)
{
    fprintf(stderr, qstr2str(QString("test copyPath: %1\n").arg(test->copyPath("./test_copyPath", "../test_copy"))));
}

void test_findStringInFile(QInstallDistro *test)
{
    fprintf(stderr, qstr2str(QString("test findStringInFile: %1\n").arg(test->findStringInFile("./tests.cpp", "void test_countFiles"))));
}

void test_replaceStringInFile(QInstallDistro *test)
{
    QFile file("prova.txt");
    file.open(QIODevice::WriteOnly);
    file.write(QByteArray("Questo è un test,\nsolo un maledetto test.\n"));
    file.close();
    file.open(QIODevice::ReadOnly);
    fprintf(stderr, file.readAll().data());
    file.close();
    fprintf(stderr, qstr2str(QString("test replaceStringInFile:\n")));
    QString str = test->findStringInFile("prova.txt", "solo un maledetto");
    test->replaceStringInFile("./prova.txt", str, "solo un stronzo di test.");
    file.open(QIODevice::ReadOnly);
    fprintf(stderr, file.readAll().data());
    file.close();
}

void test_cryptPassword(QInstallDistro *test)
{
    std::cerr << test->cryptPassword(QString("devuan")).data() << '\n';
}

void test_getListUsers(QInstallDistro *test)
{
    QString user("marcello");
    QStringList sl = test->getListUsers("");
    qDebug() << sl.join(",") << '\n';
    foreach(QString u, sl)
    {
        if (user.compare(u) == 0)
        {
            qDebug() << u << '\n';
        }
    }
}

int main(int argc, char** argv)
{
    QInstallDistro test(argc, argv);
    QInstallDistro* ptrTest = &test;
    //test_countFiles(ptrTest);
    test_copyPath(ptrTest);
    //test_findStringInFile(ptrTest);
    //test_replaceStringInFile(ptrTest);
    //test_cryptPassword(ptrTest);
    //test_getListUsers(ptrTest);
    return 0;
}
