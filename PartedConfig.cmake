# - Find the Xxf86vm include file and library
#

SET(Parted_INC_SEARCH_PATH
    /usr/include/parted
    /usr/local/include/parted
    /usr/include)

SET(Parted_LIB_SEARCH_PATH
    /usr/lib
    /usr/local/lib)


FIND_PATH(Parted_INCLUDE_DIR parted.h
          ${Parted_INC_SEARCH_PATH})

FIND_LIBRARY(Parted_LIBRARIES NAMES parted PATH ${Parted_LIB_SEARCH_PATH})

IF (Parted_INCLUDE_DIR AND Parted_LIBRARIES)
    SET(Parted_FOUND TRUE)
ENDIF (Parted_INCLUDE_DIR AND Parted_LIBRARIES)

IF (Parted_FOUND)
    INCLUDE(CheckLibraryExists)

    CHECK_LIBRARY_EXISTS(${Parted_LIBRARIES}
                        "ped_device_probe_all"
                        ${Parted_LIBRARIES}
                        Parted_HAS_CONFIG)

    IF (NOT Parted_HAS_CONFIG AND Parted_FIND_REQUIRED)
        MESSAGE(FATAL_ERROR "Could NOT find parted")
    ENDIF (NOT Parted_HAS_CONFIG AND Parted_FIND_REQUIRED)
ENDIF (Parted_FOUND)

MARK_AS_ADVANCED(
    Parted_INCLUDE_DIR
    Parted_LIBRARIES
    )
