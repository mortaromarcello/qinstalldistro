<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>QInstallDistro</name>
    <message>
        <location filename="../qinstalldistro.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Finestra principale</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.ui" line="32"/>
        <source>The options marked with an asterisk are required</source>
        <translation>Le opzioni segnate con un asterisco sono necessarie</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.ui" line="44"/>
        <source>User password (*)</source>
        <translation>Password utente (*)</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.ui" line="60"/>
        <source>Home partition</source>
        <translation>Partizione home</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.ui" line="70"/>
        <source>Installation drive (*)</source>
        <translation>Disco di installazione (*)</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.ui" line="77"/>
        <source>Root partition (*)</source>
        <translation>Partizione root (*)</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.ui" line="87"/>
        <source>Language</source>
        <translation>Linguaggio</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.ui" line="94"/>
        <source>Keyboard</source>
        <translation>Tastiera</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.ui" line="101"/>
        <source>Hostname (*)</source>
        <translation>Nome host (*)</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.ui" line="111"/>
        <source>Groups (*)</source>
        <translation>Gruppi (*)</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.ui" line="121"/>
        <source>Timezone</source>
        <translation>Timezone</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.ui" line="131"/>
        <source>Username (*)</source>
        <translation>Nome utente (*)</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.ui" line="152"/>
        <source>Root password (*)</source>
        <translation>Password di root (*)</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.ui" line="162"/>
        <source>Locale (*)</source>
        <translation>Locale (*)</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.ui" line="169"/>
        <source>Shell</source>
        <translation>Shell</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.ui" line="179"/>
        <source>Use home partition</source>
        <translation>Usa la partizione home</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.ui" line="229"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.ui" line="238"/>
        <source>&amp;Exit</source>
        <translation>&amp;Esci</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="221"/>
        <source>QinstallDistro</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="247"/>
        <source>Values checked not are correct.</source>
        <translation>I valori non sono corretti.</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="302"/>
        <source>Create and mount root partition</source>
        <translation>Crea e monta la partizione root</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="307"/>
        <location filename="../qinstalldistro.cpp" line="338"/>
        <source>mkfs command returned error code %1.
</source>
        <translation>Il comando mkfs ritorna un codice di errore %1.
</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="314"/>
        <location filename="../qinstalldistro.cpp" line="346"/>
        <source>mkdir command returned error code %1.
</source>
        <translation>Il comando mkdir ritorna un codice di errore %1.
</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="320"/>
        <location filename="../qinstalldistro.cpp" line="352"/>
        <source>mount command returned error code %1.
</source>
        <translation>Il comando mount ritorna un codice di errore %1.
</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="332"/>
        <source>Create Home and mount partition</source>
        <translation>Crea e monta la partizione home</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="365"/>
        <source>Create swap partition</source>
        <translation>Crea la partizione swap</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="369"/>
        <source>mkswap command returned error code %1.
</source>
        <translation>Il comando mkswap ritorna un codice di errore %1.
</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="380"/>
        <source>Copy root</source>
        <translation>Copia root</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="400"/>
        <source>useradd command returned error code %1.
</source>
        <translation>Il comando useradd ritorna un codice di errore %1.
</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="518"/>
        <source>Install grub</source>
        <translation>Installo grub</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="567"/>
        <source>Root partition is empty</source>
        <translation>La partizione root è vuota</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="601"/>
        <source>Installation drive is empty</source>
        <translation>Il disco di installazione non c&apos;è</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="611"/>
        <source>Username is empty</source>
        <translation>Il nome utente è vuoto</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="628"/>
        <source>User password is empty</source>
        <translation>La password utente è vuota</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="638"/>
        <source>Root password is empty</source>
        <translation>La password root è vuota</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="647"/>
        <source>Locale is empty</source>
        <translation>Il campo Locale è vuoto</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="655"/>
        <source>Hostname is empty</source>
        <translation>Il campo nome host è vuoto</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="663"/>
        <source>Groups is empty</source>
        <translation>il campo gruppi è vuoto</translation>
    </message>
    <message>
        <location filename="../qinstalldistro.cpp" line="856"/>
        <source>Information</source>
        <translation>Informazioni</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="15"/>
        <source>you are root!
</source>
        <translation>Tu sei root!</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="18"/>
        <source>you are not root...
</source>
        <translation>Tu non sei root...</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="19"/>
        <source>You are not root!</source>
        <translation>Tu non sei root!</translation>
    </message>
</context>
</TS>
