TEMPLATE = app
QT += widgets
SOURCES += main.cpp parted.cpp qinstalldistro.cpp scrollmessagebox.cpp
HEADERS += config.h parted.h qinstalldistro.h mydebug.h qdebugstream.h scrollmessagebox.h
FORMS = qinstalldistro.ui
LIBS += -lparted -lblkid -lcrypt -lboost_filesystem -lboost_system
TRANSLATIONS = translations/qinstalldistro_it.ts
RESOURCES += qinstalldistro.qrc
# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =
# Default rules for deployment.
#include(deployment.pri)
